# Recipe App API Proxy

NGNIX proxy app for our recipe app API

## Usage

### Enviroment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST`- Hostname of the app to forward requests to (default:`app`)
